import React, { Component } from 'react'
import { compose } from 'recompose'
import stores from './store_directory_complete';
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow } from 'react-google-maps'

import './YourComponent.css';

const MapWithAMarker = compose(withScriptjs, withGoogleMap)(props => {
  return (
    <GoogleMap defaultZoom={5} defaultCenter={props.defaultCenter}>
      {props.markers.map((marker, index) => {
        const onClick = props.onClick.bind(this, marker)
        return (
          <Marker
            key={'Mark-' + index}
            onClick={onClick}
            position={marker.location}
          >
            {props.selectedStore === marker &&
              <InfoWindow>
                <div>
                  <h2>{marker.Name}</h2>
                  <div>
                    <p>{marker.Address}</p>
                    <img
                      src="https://img.filesoul.com/img/icon/star_downloader_384.png"
                      onClick={props.setFavorite.bind(this, index)}
                    />
                  </div>
                </div>
              </InfoWindow>}

          </Marker>
        )
      })}
    </GoogleMap>
  )
})

export default class YourComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      stores: [],
      selectedStore: false,
      favoriteStores: []
    }
    this.mexicoLatLng = {
      lat: 24.397,
      lng: -99.644
    };
    this.styleFavorites = {
      marginBottom: '50px'
    }
  }
  componentDidMount = () => {
    this.setState({ stores: stores })
  }

  _onClickMarker = (marker) => {
    this.setState({ selectedStore: marker });
  }

  _onSetFavorite = (indexStore) => {
    let store = stores[indexStore];
    console.log(indexStore, store);
    console.log(this.state.favoriteStores);
    let checkStore = this.state.favoriteStores.find(item => item.Name === store.Name);
    if (!checkStore) {
      this.setState({ favoriteStores: [...this.state.favoriteStores, store] });
    }
  }

  _onDeleteFromFavorites = (nameStore) => {
    console.log(nameStore);
    let cloneStores = this.state.favoriteStores;
    let checkStore = cloneStores.find(item => item.Name === nameStore);
    let index = cloneStores.indexOf(checkStore);
    if (index > -1) {
      cloneStores.splice(index, 1);
    }
    this.setState({ favoriteStores: cloneStores });
  }

  render = () => {
    return (
      <div>
        <MapWithAMarker
          defaultCenter={this.mexicoLatLng}
          selectedStore={this.state.selectedStore}
          markers={this.state.stores}
          onClick={this._onClickMarker}
          setFavorite={this._onSetFavorite}
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVH8e45o3d-5qmykzdhGKd1-3xYua5D2A&v=3.exp"
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `400px` }} />}
          mapElement={<div style={{ height: `100%` }} />}
        />
        <div style={this.styleFavorites}>
          {this.state.favoriteStores.map((store, index) => {
            return (
              <div className="favorite-store">
                <p key={'FS-' + index}>{store.Name}</p>
                <button key={'DFS-' + index} onClick={this._onDeleteFromFavorites.bind(this, store.Name)}>Quit</button>
              </div>
            )
          })}
        </div>
      </div>
    )
  }
}