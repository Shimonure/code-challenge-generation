import React, { Component } from 'react';
import stores from './store_directory';

/*
* Use this component as a launching-pad to build your functionality.
*
*/
class GenerateComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      mapLoaded: false,
      storesComplete: []
    };
    this.storesComplete = [];
    this.mexicoLatLng = {
      lat: 24.397,
      lng: -99.644
    };
    this.init = 260;
    this.finish = 273;
    this.styleMap = {
      width: '100vw',
      height: '100vh'
    };
    //this.storesComplete = [];
  }

  componentDidMount() {
    const ApiKey = 'AIzaSyCVH8e45o3d-5qmykzdhGKd1-3xYua5D2A';
    const script = document.createElement('script');
    script.src = `https://maps.googleapis.com/maps/api/js?key=${ApiKey}`;
    script.async = true;
    script.defer = true;
    script.addEventListener('load', () => {
      this.setState({ mapLoaded: true });
    });

    document.body.appendChild(script);
  }

  componentDidUpdate() {
    if (this.state.mapLoaded) {
      // Render the map
      this.map = new window.google.maps.Map(document.getElementById('map'), {
        center: this.mexicoLatLng,
        zoom: 5
      });

      // Set markers into the map
      for (var i = this.init; i < this.finish; ++i) {
        this.doSetTimeout(i);
      }
    }
  }

  doSetTimeout(i) {
    setTimeout(function () {
      this.getGeocoder(stores[i].Address, response => {
        // console.log(response);
        if (response.sucess) {
          console.log('Done', i, stores[i].Address, response);
          let store = stores[i];
          store.location = response.data;
          new window.google.maps.Marker({ position: response.data, map: this.map });
          this.storesComplete.push(store);

          if (i == this.finish - 1) {
            console.log('object');
            console.log(this.storesComplete);
            //I use this LOG for copy and pasto to the JSON with complete information
            console.log(JSON.stringify(this.storesComplete));
          }

        }
        else {
          console.log('ERROR', i, stores[i].Address, response);
          if (response.status = 'OVER_QUERY_LIMIT') {
            console.log('OVER_QUERY_LIMIT', i);
          }
        }
      });
    }.bind(this), 1500 * (i - this.init));
  }


  /**
   * Get the Lat and Lang of first result using Geocoder
   * 
   * @param addres string
   * @param callback
   */
  getGeocoder(address, callback) {
    let geocoder = new window.google.maps.Geocoder();
    if (address) {
      geocoder.geocode({ 'address': address }, function (results, status) {
        // console.log(results,status);
        if (status == 'OK') {
          let data = {
            lat: results[0].geometry.location.lat(),
            lng: results[0].geometry.location.lng()
          }
          callback({ sucess: true, data: data, status: status });
        }
        else {
          let menssage = 'Geocode was not successful for the following reason: ' + status;
          callback({ sucess: false, menssage: menssage, status: status });
        }
      });
    }
  }



  render() {
    return (<div>
      <div style={this.styleMap} id="map" />
    </div>
    );
  }
}


export default GenerateComponent;