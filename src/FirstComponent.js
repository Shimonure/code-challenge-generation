import React, { Component } from 'react';
import stores from './store_directory_complete';

/*
* Use this component as a launching-pad to build your functionality.
*
*/
class FirstComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      mapLoaded: false,

    };
    this.mexicoLatLng = {
      lat: 24.397,
      lng: -99.644
    };
    this.styleMap = {
      width: '100vw',
      height: '100vh'
    };
    this.infoWindows = [];
  }

  componentDidMount() {
    const ApiKey = 'AIzaSyCVH8e45o3d-5qmykzdhGKd1-3xYua5D2A';
    const script = document.createElement('script');
    script.src = `https://maps.googleapis.com/maps/api/js?key=${ApiKey}`;
    script.async = true;
    script.defer = true;
    script.addEventListener('load', () => {
      this.setState({ mapLoaded: true });
    });

    document.body.appendChild(script);


  }

  componentDidUpdate() {
    if (this.state.mapLoaded) {
      // Render the map
      this.map = new window.google.maps.Map(document.getElementById('map'), {
        center: this.mexicoLatLng,
        zoom: 5
      });

      // Set markers into the map
      for (var i = 0; i < stores.length; ++i) {
        this.doSetTimeout(i)
      }
    }


  }

  doSetTimeout(i) {
    setTimeout(function () { 
      this.setMarker(i); 
    }.bind(this), 100);
  }

  setMarker(i) {

    console.log(i, stores[i].Name);
    var contentString = '<div id="content">' +
      '<div id="siteNotice">' +
      '</div>' +
      '<h1 id="firstHeading" class="firstHeading">' + stores[i].Name + '</h1>' +
      '<div id="bodyContent">' +
    '<p>' + stores[i].Address + '</p>' +
    '<button "onClick={this.handleClick}">load content</button>' +
    '</div>' +
      '</div>';

    var infowindow = new window.google.maps.InfoWindow({
      content: contentString
    });

    this.infoWindows.push(infowindow);

    var marker = new window.google.maps.Marker({
      position: stores[i].location,
      map: this.map,
      title: 'Store Information'
    });

    marker.addListener('click', function () {
      this.closeAllInfoWindows();
      infowindow.open(this.map, marker);
    }.bind(this));

  }

  handleClick(value){
    console.log('click');
    console.log(value);
  }


  closeAllInfoWindows(){
    for (var i=0;i<this.infoWindows.length;i++) {
      this.infoWindows[i].close();
    }
  }

  render() {
    return (<div>
      <div style={this.styleMap} id="map" />
    </div>
    );
  }
}


export default FirstComponent;