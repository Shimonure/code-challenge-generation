import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Route } from 'react-router-dom';
import './App.css';

import InstructionComponet from "./InstructionComponet";
import FirstComponent from './FirstComponent'
import GenerateComponent from "./GenerateComponent";
import YourComponent from './YourComponent';

class App extends Component {
  render() {
    return (
      <div>
      <Link  to="/instructions">Instructions</Link>
   
      
      <YourComponent></YourComponent>
      <Route path="/instructions" component={InstructionComponet} />

      </div>
      
    );
  }
}

export default App;
